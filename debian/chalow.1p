.TH CHALOW 1p "July  1, 2006"
.SH NAME
chalow - weblog tool that converts ChangeLog to HTML
.\"
.SH SYNOPSIS
.B chalow
[\fIOPTION\fR]... \fICHANGELOG\fR...
.\"
.SH DESCRIPTION
.B chalow
is a weblog tool written in Perl.
It converts ChangeLog to HTML and RSS.
.PP
The options are as follows:
.TP
\fB\-n, \-\-top-n\fR=\fINUM\fR
write
.IR NUM
days to index.html
.TP
\fB\-o, \-\-output-dir\fR=\fIDIR\fR
directory to output
.TP
\fB\-c, \-\-configure-file\fR=\fIFILE\fR
configure file
.TP
\fB\-s, \-\-stop-date\fR=\fIDATE\fR
date to stop processing
.TP
\fB\-u, \-\-update-by-size\fR
overwrite only if sizes are different
.TP
\fB\-C, \-\-css\fR=\fIFILE\fR
css file
.TP
\fB\-q, \-\-quiet\fR
quiet mode
.TP
\fB\-8, \-\-utf8\fR
utf8 mode
.TP
\fB\-d, \-\-debug\fR
debug mode
.\"
.SH EXAMPLES
.SS Generate index.html, cl.rdf, etc. from ChangeLog:
.na
.nf
chalow ChangeLog
.fi
.ad
.SS Generate index.html, cl.rdf, etc. from ChangeLog with cl.conf:
.na
.nf
chalow -c cl.conf ChangeLog
.fi
.ad
.SS Generate index.html, cl.rdf, etc. into the $HOME/public_html/diary directory from ChangeLog* (such as ChangeLog, ChangeLog.2, ChangeLog.old) with cl.conf in utf8 mode:
.na
.nf
chalow --utf8 -o $HOME/public_html/diary -c cl.conf ChangeLog*
.fi
.ad
.\"
.SH BUGS
.B chalow
assumes that ChangeLog file is encoded with EUC-JP or US-ASCII by default.
If you want to use UTF-8, use the utf8 mode option (`-8' or `--utf8').
Other encodings, such as ISO-8859-1, ISO-8859-15, EUC-KR, are not supported.
.PP
This manual page is provided by Debian.  It is not integrated in the upstream source.
.\"
.SH SEE ALSO
More information is available at `/usr/share/doc/chalow' on Debian systems.
